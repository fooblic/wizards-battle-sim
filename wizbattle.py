#!/usr/bin/python3
# -*- coding: utf-8 -*-
''' 
Wizards.one battle simulator
'''
'''
Blockchain game for EOS holders:
https://game.wizard.one

Simulator based on description from article:
https://steemit.com/eos/@wizards.one/wizards-battle-dna-description
'''
import math
import numpy as np

print(__doc__)

MU = 50
SIGMA = 8
DNA = 8  # DNA nums
LOGS = False  # print debug info
ROUNDS = 100 * 1000  # repeat battle

def generate_dna():
    ''' Generate Normal distribution '''
    d_list = []
    rnd = np.random.normal(MU, SIGMA, DNA)

    # convert float to integer
    for item in range(DNA):
        d_list.append(int(round(rnd[item], 0)))
    return d_list

def rmsd(d_list, center):
    ''' Root-mean-square deviation '''
    dna = np.array(d_list)
    result = np.sqrt(np.mean(((np.array(dna) - center)**2)))
    return result

def bias(d_list, center):
    ''' Mean deviation '''
    dna = np.array(d_list)
    result = np.mean(np.abs(np.array(dna) - center))
    return result


class Wizard():
    ''' Wizard object description '''
    def __init__(self, name, dna_list):
        self.name = name
        self.dna = dna_list
        self.damage = 0
        self.attack = []
        self.resist = []

        for item in range(DNA):
            delta = self.dna[item] - MU
            if delta >= 0:
                self.attack.append(SIGMA + delta)
                self.resist.append(math.floor(SIGMA + (delta / 2)))
            else:
                self.attack.append(math.ceil(SIGMA - (delta / 2)))
                self.resist.append(SIGMA - delta)

    def score(self):
        print("%s\t %s" % (self.name, self.damage))

    def show(self):
        print("\n%s: %s -> bias: %.3f" % (self.name, self.dna, bias(self.dna, MU)),
              "\nMean: %.2f  RMSD: %.2f" % (np.mean(self.dna), rmsd(self.dna, MU)),
              "\nAttack: %s" % self.attack, "Mean: %.2f RMSD: %.2f" %
              (np.mean(self.attack), rmsd(self.attack, 0)),
              "\nResist: %s" % self.resist, "Mean: %.2f RMSD: %.2f" %
              (np.mean(self.resist), rmsd(self.resist, 0)))



class Battle():
    ''' Battle of two wizards '''
    def __init__(self, wizard1, wizard2):

        self.wiz1 = wizard1
        self.wiz2 = wizard2

        self.first_name = wizard1.name
        self.second_name = wizard2.name

        self.wizards = {}
        self.wizards[self.first_name] = wizard1
        self.wizards[self.second_name] = wizard2


    def random_damage(self):

        rnd = np.random.randint(2)
        if rnd == 0:
            self.wiz1.damage += 1
            loss_name = self.first_name
        else:
            self.wiz2.damage += 1
            loss_name = self.second_name

        if LOGS:
            print("## Random damage")
            print(loss_name, "loss: 1")

    def magic_fight_1(self, first_name, second_name, a2x):

        if a2x:
            coef = 2
        else:
            coef = 1

        # select magic - 1 variant
        magic = np.random.randint(8)

        # choice magic attack and resist
        amax = self.wizards[first_name].attack[magic]
        rmax = self.wizards[second_name].resist[magic]
        attack = np.random.randint(amax + 1) * coef
        resist = np.random.randint(rmax + 1)

        # attack result
        if attack <= resist:
            loss = 0

        elif attack > (resist + SIGMA):
            loss = 2
            self.wizards[second_name].damage += loss

        elif attack > resist:
            loss = 1
            self.wizards[second_name].damage += loss
        else:
            print("ERROR 1")
            return

        if LOGS:
            print("### Magic fight", first_name, " vs. ", second_name)
            print(self.wizards[first_name].attack)
            print(self.wizards[second_name].resist)
            print(magic + 1, amax, rmax)
            print(first_name, attack, "->", resist, second_name)
            print("%s loss: %s" % (second_name, loss))

    def magic_fights(self, first_name, second_name):

        attack2x = False

        for each in range(4):
            if LOGS:
                print("## Attack", each + 1)
            self.magic_fight_1(first_name, second_name, attack2x)
            self.magic_fight_1(second_name, first_name, attack2x)

        attack2x = True
        if LOGS:
            print("## Attack", 5)
        self.magic_fight_1(first_name, second_name, attack2x)
        self.magic_fight_1(second_name, first_name, attack2x)

    def show_damage(self):
        print("\n=== Damage")
        self.wiz1.score()
        self.wiz2.score()

    def winner(self):
        if self.wiz1.damage > self.wiz2.damage:  # the one is more loss 
            the_winner = self.second_name

        elif self.wiz2.damage > self.wiz1.damage:
            the_winner = self.first_name
            
        else:
            the_winner = ""
            if LOGS:
                print("\nNo winner")

        if LOGS and the_winner:
            print("\n=== The winner %s !!!" % the_winner)

        return the_winner


class Simulation():
    ''' Start battle simulation 'num' times'''
    def __init__(self, battle, num):
        self.battle = battle
        self.num = num
        self.score = {}

        self.score[self.battle.first_name] = 0
        self.score[self.battle.second_name] = 0
        self.score["No winner"] = 0

    def start(self):

        # 1st phase - random damage
        self.battle.random_damage()

        if LOGS:
            battle1.show_damage()

        # 2nd phase - magic fights
        self.battle.magic_fights(self.battle.first_name, self.battle.second_name)

        if LOGS:
            battle1.show_damage()

        # 3nd phase - winner
        winner = self.battle.winner()
        if winner:
            self.score[winner] += 1
        else:
            self.score["No winner"] += 1

    def battling(self):
        print("Starting %s rounds..." % self.num)
        for item in range(self.num):
            if LOGS:
                print("\n# Round %s =========== " % (item + 1))
            self.battle.wiz1.damage = 0
            self.battle.wiz2.damage = 0
            self.start()

    def results(self):
        print("TOTAL:\n", self.score)

        total_wins = self.score[self.battle.first_name] + self.score[self.battle.second_name]
        
        if total_wins != 0:
            win_rate = self.score[self.battle.first_name] / total_wins * 100
            print("%s win rate: %s%%" % (self.battle.first_name, round(win_rate, 1)))

        no_win = self.score["No winner"] / self.num * 100
        print("No win rate: %s" % round(no_win, 1))

        mean1 = np.mean(self.battle.wiz1.attack + self.battle.wiz1.resist)
        mean2 = np.mean(self.battle.wiz2.attack + self.battle.wiz2.resist)
        print("Mean ratio: %.2f / %.2f (%.2f)" % (mean1, mean2, mean1/mean2))


wiz1 = Wizard("Wizard '272'", [54, 39, 45, 56, 44, 34, 41, 47])
wiz1.show()

wiz2 = Wizard("Wizard RAND", generate_dna())
wiz2.show()

battle1 = Battle(wiz1, wiz2)

simul = Simulation(battle1, ROUNDS)
simul.battling()
simul.results()



